BioconductorCRAN2spec purpose is to convert a R library DESCRIPTION file,
remotely taken from Bioconductor or CRAN, into an RPM spec file.

The spec file follows RedHat 6 specifications.

Sebastien Moretti from the Vital-IT (https://www.vital-it.ch) Center
for high-performance computing of the SIB Swiss Institute of Bioinformatics.
